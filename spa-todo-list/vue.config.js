const WebpackAssetsManifest = require('webpack-assets-manifest')

module.exports = {
    runtimeCompiler: true,
    configureWebpack: config => {
        config.plugins = config.plugins.concat(
            new WebpackAssetsManifest({
                output: 'manifest.json'
            })
        )
    }
}