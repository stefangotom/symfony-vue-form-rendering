export default function slotWrapperComponent(slotFn) {
    return {
        props: ['formData', 'formStructure', 'formRegistry'],
        data() {
            return {
                data: this.formData,
                structure: this.formStructure,
                registry: this.formRegistry,
            };
        },
        render(createElement) {
            const props = {
                formData: this.data,
                formStructure: this.structure,
                formRegistry: this.registry,
                updateValue: (field, value) => {
                    if (field !== null && this.structure.vars.compound) {
                        this.data = {...this.data, [field]: value};
                    } else {
                        this.data = value;
                    }

                    this.$emit('input', this.data);
                }
            };

            const vnodes = slotFn(props);

            if (vnodes.length === 1) {
                return vnodes[0];
            }

            return createElement('div', vnodes);
        }
    }
}