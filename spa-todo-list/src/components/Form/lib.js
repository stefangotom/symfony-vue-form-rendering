export function reverse(array) {
    return [...array].reverse();
}

export function extractFormDataTree(form, top = false) {
    const result  = {};

    let children = Object.values(form.children);

    for (let child of children)
    {
        if (child.vars.compound && child.children) {
            result[child.vars.name] = extractFormDataTree(child, false);
        } else {
            result[child.vars.name] = child.vars.value;
        }
    }

    if (top) {
        return {[form.vars.name]: result};
    }

    return result;
}

export function replacePlaceholderInPrototype(prototype, index, isTopLevel = true) {
    const placeholderName = /__name__/; // regex because we only want to replace the *first* occurrence
    const fields = new Set(['id', 'full_name', ...(isTopLevel ? ['name'] : [])]);

    const result = {vars: {}};
    for (const key of Object.keys(prototype.vars)) {
        let value = prototype.vars[key];

        if (fields.has(key)) {
            value = value.replace(placeholderName, index);
        } else if(key === 'prototype') {
            value = replacePlaceholderInPrototype(value, index, false);
        }
        result.vars[key] = value;
    }

    if (!prototype.children) {
        return result;
    }

    result.children = {};

    for (const key of Object.keys(prototype.children)) {
        result.children[key] = replacePlaceholderInPrototype(prototype.children[key], index, false);
    }

    return result;
}

export function getNextIndexForStructure(structure) {
    if (!structure.children) {
        return 0;
    }

    return Object.keys(structure.children).reduce((lastIndex, index) => {
        const numIndex = parseInt(index, 10);

        if (isNaN(numIndex)) {
            return lastIndex;
        }

        return Math.max(lastIndex, numIndex) + 1;
    }, 0);
}