import Label from "./Label";
import FormRow from "./FormRow";
import Form from "./Form";
import Text from "./Text";
import Hidden from "./Hidden.vue";
import Choice from "./Choice";
import Collection from "./Collection";
import Errors from "./Errors";
import AjaxChoice from "./AjaxChoice";

export const FormRegistry = {
    'form_label': Label,
    'form_row': FormRow,
    'form_widget': Form,
    'text_widget': Text,
    'number_widget': Text,
    'hidden': Hidden,
    'choice_widget': Choice,
    'collection_widget': Collection,
    'form_error': Errors,
    'ajax_choice_type_widget': AjaxChoice,
};