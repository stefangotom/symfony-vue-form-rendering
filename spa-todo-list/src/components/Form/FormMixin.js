import {reverse} from "./lib";
import slotWrapperComponent from "./SlotWrapper";

export const FormMixin = {
    props: {
        formData: {
            required: true,
        },
        formStructure: {
            type: Object,
            required: true,
        },
        formRegistry: {
            type: Object,
            required: false,
            default: () => ({}),
        }
    },
    data() {
        return {
            structure: this.formStructure,
            data: this.formData,
        };
    },
    watch: {
        formData(newData) {
            this.data = newData;
        },
        formStructure(newStructure) {
            this.structure = newStructure;
        }
    },
    computed: {
        formLabelComponent() {
            return this.lookupComponent(this.structure.vars.block_prefixes, 'label');
        },
        formWidgetComponent() {
            return this.lookupComponent(this.structure.vars.block_prefixes, 'widget');
        },
        formErrorsComponent() {
            return this.getComponent('form_error');
        },
        registry() {
            return {...this.$formRegistry, ...this.formRegistry, ...this.fromSlots(this.$scopedSlots) };
        },
    },
    methods: {
        fromSlots(slots) {
            return Object.fromEntries(Object.entries(slots).map(([key, slot]) => {
                return [key, slotWrapperComponent(slot)];
            }));
        },
        updateValue(field, value) {
            if (field !== null && this.structure.vars.compound) {
                this.data = {...this.data, [field]: value};
            } else {
                this.data = value;
            }

            this.$emit('input', this.data);
        },
        getComponent(name) {
            const formRegistry = this.$formRegistry;

            if (Object.hasOwnProperty.call(formRegistry, name)) {
                return formRegistry[name];
            }

            return undefined;
        },
        lookupComponent(blockPrefixes, suffix) {
            const formRegistry = this.registry;

            for(let blockPrefix of reverse(blockPrefixes)) {
                const blockName = `${blockPrefix}_${suffix ? suffix : ''}`;
                if (Object.hasOwnProperty.call(formRegistry, blockName)) {
                    return formRegistry[blockName];
                }

                if (suffix && Object.hasOwnProperty.call(formRegistry, blockPrefix)) {
                    return formRegistry[blockPrefix];
                }
            }

            return undefined;
        },
    },
}