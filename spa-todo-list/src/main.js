import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import VueAxios from "vue-axios";
import VueRouter from 'vue-router';
// import {RouteConfig} from "vue-router";
import TodoLists from "./views/TodoLists";
import TodoList from "./views/TodoList";
import SfForm from "./components/SfForm";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import VueCustomElement from 'vue-custom-element';


import './style.scss';
import {FormRegistry} from "./components/Form/FormRegistry";
import ReactiveTest from "./views/ReactiveTest";
import TestView from "./views/TestView";
import FormTest from "./views/FormTest";
import SelectInput from "./components/SelectInput";

const appAxios = axios.create({
  headers: {
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
  },
});




const VueFormPlugin = {};
VueFormPlugin.install = function(Vue) {
  Vue.prototype.$formRegistry = FormRegistry;
}

Vue.use(VueFormPlugin);


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.use(VueRouter)
Vue.use(VueAxios, appAxios);
Vue.config.productionTip = false


Vue.use(VueCustomElement)

Vue.customElement('sf-form', SfForm);


Vue.component('test-view', TestView);
Vue.component('select-input', SelectInput);

if (document.querySelector('#app')) {
  // void RouteConfig;
  /** @type {RouteConfig[]} */
  const routes = [
    {path: '/', redirect: '/todo'},
    {path: '/todo', component: TodoLists},
    {path: '/todo/:id', component: TodoList, props: true},
    {path: '/reactive-test', component: ReactiveTest },
    {path: '/form-test', component: FormTest },
  ];

  const router = new VueRouter({
    routes,
  })

  new Vue({
    router,
    render: h => h(App),
  }).$mount('#app')
}

if (document.querySelector('#test-root')) {
  new Vue({
    el: '#test-root'
  });
}

const createComponent = (component, elem) => {
  new Vue({component}).$mount(elem);
};

const componentThings = document.querySelectorAll('component[is]');

console.log('components', Vue.options.components);

componentThings.forEach(function(elem) {
  const components = Vue.options.components;
  const componentName = elem.getAttribute('is');

  if (!(componentName in components)) {
    console.error(`Couldn't find component for name "${ componentName }"`, elem);
    return;
  }

  createComponent(components[componentName], elem);
})