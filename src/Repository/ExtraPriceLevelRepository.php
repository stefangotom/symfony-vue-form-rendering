<?php

namespace App\Repository;

use App\Entity\ExtraPriceLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraPriceLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraPriceLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraPriceLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ExtraPriceLevel[]    findAll()
 */
class ExtraPriceLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraPriceLevel::class);
    }
}