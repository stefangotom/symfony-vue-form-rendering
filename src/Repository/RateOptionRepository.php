<?php

namespace App\Repository;

use App\Entity\RateOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RateOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method RateOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method RateOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method RateOption[]    findAll()
 */
class RateOptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RateOption::class);
    }
}