<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestLocaleSubscriber implements EventSubscriberInterface
{
    private array $allowedLocales;

    public function __construct(array $requestListenerAllowedLocales = [])
    {
        $this->allowedLocales = $requestListenerAllowedLocales;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        $this->setLocale($request);
    }

    private function setLocale(Request $request): void
    {
        if ($preferredLocale = $request->getPreferredLanguage($this->allowedLocales)) {
            $request->setLocale($preferredLocale);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                // Run before LocaleListener, so _locale router attribute can override
                // The Accept-Language header
                ['onKernelRequest', 17],
            ],
        ];
    }
}