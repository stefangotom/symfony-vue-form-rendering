<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210505191207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SEQUENCE col_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tables_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cell_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE row_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE col (id INT NOT NULL, table_id INT NOT NULL, col_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_13B1F670ECFF285C ON col (table_id)');
        $this->addSql('CREATE TABLE tables (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE cell (id INT NOT NULL, table_id INT NOT NULL, row_id INT NOT NULL, column_id INT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CB8787E2ECFF285C ON cell (table_id)');
        $this->addSql('CREATE INDEX IDX_CB8787E283A269F2 ON cell (row_id)');
        $this->addSql('CREATE INDEX IDX_CB8787E2BE8E8ED5 ON cell (column_id)');
        $this->addSql('CREATE TABLE row (id INT NOT NULL, table_id INT NOT NULL, row_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8430F6DBECFF285C ON row (table_id)');
        $this->addSql('ALTER TABLE col ADD CONSTRAINT FK_13B1F670ECFF285C FOREIGN KEY (table_id) REFERENCES tables (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cell ADD CONSTRAINT FK_CB8787E2ECFF285C FOREIGN KEY (table_id) REFERENCES tables (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cell ADD CONSTRAINT FK_CB8787E283A269F2 FOREIGN KEY (row_id) REFERENCES row (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cell ADD CONSTRAINT FK_CB8787E2BE8E8ED5 FOREIGN KEY (column_id) REFERENCES col (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE row ADD CONSTRAINT FK_8430F6DBECFF285C FOREIGN KEY (table_id) REFERENCES tables (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE cell DROP CONSTRAINT FK_CB8787E2BE8E8ED5');
        $this->addSql('ALTER TABLE col DROP CONSTRAINT FK_13B1F670ECFF285C');
        $this->addSql('ALTER TABLE cell DROP CONSTRAINT FK_CB8787E2ECFF285C');
        $this->addSql('ALTER TABLE row DROP CONSTRAINT FK_8430F6DBECFF285C');
        $this->addSql('ALTER TABLE cell DROP CONSTRAINT FK_CB8787E283A269F2');
        $this->addSql('DROP SEQUENCE col_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tables_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cell_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE row_id_seq CASCADE');
        $this->addSql('DROP TABLE col');
        $this->addSql('DROP TABLE tables');
        $this->addSql('DROP TABLE cell');
        $this->addSql('DROP TABLE row');
    }
}
