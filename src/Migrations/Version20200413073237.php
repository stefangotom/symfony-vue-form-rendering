<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200413073237 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SEQUENCE rate_option_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE price_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE extra_price_level_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE price_group_price_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rate_option (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE price_group (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE extra_price_level (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE price_group_price (id INT NOT NULL, price_group_id INT NOT NULL, rate_option_id INT NOT NULL, price_level_id INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9D3D45A89CE2E250 ON price_group_price (price_group_id)');
        $this->addSql('CREATE INDEX IDX_9D3D45A86C25901 ON price_group_price (rate_option_id)');
        $this->addSql('CREATE INDEX IDX_9D3D45A83D0770B0 ON price_group_price (price_level_id)');
        $this->addSql('ALTER TABLE price_group_price ADD CONSTRAINT FK_9D3D45A89CE2E250 FOREIGN KEY (price_group_id) REFERENCES price_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE price_group_price ADD CONSTRAINT FK_9D3D45A86C25901 FOREIGN KEY (rate_option_id) REFERENCES rate_option (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE price_group_price ADD CONSTRAINT FK_9D3D45A83D0770B0 FOREIGN KEY (price_level_id) REFERENCES extra_price_level (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE price_group_price DROP CONSTRAINT FK_9D3D45A86C25901');
        $this->addSql('ALTER TABLE price_group_price DROP CONSTRAINT FK_9D3D45A89CE2E250');
        $this->addSql('ALTER TABLE price_group_price DROP CONSTRAINT FK_9D3D45A83D0770B0');
        $this->addSql('DROP SEQUENCE rate_option_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE price_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE extra_price_level_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE price_group_price_id_seq CASCADE');
        $this->addSql('DROP TABLE rate_option');
        $this->addSql('DROP TABLE price_group');
        $this->addSql('DROP TABLE extra_price_level');
        $this->addSql('DROP TABLE price_group_price');
    }
}
