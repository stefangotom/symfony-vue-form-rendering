<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class PriceGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @var Collection|PriceGroupPrice[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\PriceGroupPrice", mappedBy="priceGroup", indexBy="id")
     */
    private Collection $prices;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return PriceGroupPrice[]|Collection
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(PriceGroupPrice $price): void
    {
        $this->prices->add($price);
    }

    public function removePrice(PriceGroupPrice $price): void
    {
        $this->prices->removeElement($price);
    }
}