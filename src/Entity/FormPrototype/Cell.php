<?php

namespace App\Entity\FormPrototype;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Cell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Tables")
     * @ORM\JoinColumn(nullable=false)
     */
    private Tables $table;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FormPrototype\Row", inversedBy="cells")
     * @ORM\JoinColumn(nullable=false)
     */
    private Row $row;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FormPrototype\Col")
     * @ORM\JoinColumn(nullable=false)
     */
    private Col $column;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $value = '';

    public function __construct(Tables $table, Row $row, Col $column)
    {
        $this->table = $table;
        $this->row = $row;
        $this->column = $column;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTable(): Tables
    {
        return $this->table;
    }

    public function getRow(): Row
    {
        return $this->row;
    }

    public function getColumn(): Col
    {
        return $this->column;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function isEmpty(): bool
    {
        return $this->value === '';
    }
}