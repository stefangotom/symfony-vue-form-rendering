<?php

namespace App\Entity\FormPrototype;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Tables
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @var Row[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\FormPrototype\Row", mappedBy="table", orphanRemoval=true)
     */
    private Collection $rows;

    /**
     * @var Col[]|Collection
     * @ORM\OneToMany(targetEntity="App\Entity\FormPrototype\Col", mappedBy="table", orphanRemoval=true)
     */
    private Collection $cols;

    private string $additionalData = '';

    public function __construct()
    {
        $this->rows = new ArrayCollection();
        $this->cols = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Row[]|Collection
     */
    public function getRows(): Collection
    {
        return $this->rows;
    }

    public function addRow(Row $row): void
    {
        $this->rows->add($row);
    }

    public function removeRow(Row $row): void
    {
        $this->rows->removeElement($row);
        $row->getCells()->clear();
    }

    /**
     * @return Col[]|Collection
     */
    public function getCols(): Collection
    {
        return $this->cols;
    }

    public function addCol(Col $col): void
    {
        $this->cols->add($col);
    }

    public function removeCol(Col $col): void
    {
        $this->cols->removeElement($col);
        foreach ($this->rows as $row) {
            $cell = $row->getCellForCol($col);
            if ($cell) {
                $row->removeCell($cell);
            }
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function updateCells(): void
    {
        foreach ($this->rows as $row) {
            $untouchedCells = new \SplObjectStorage();
            foreach ($row->getCells() as $cell) {
                $untouchedCells->attach($cell, $cell);
            }

            foreach ($this->cols as $col) {
                $cell = $row->getCellForCol($col);
                if (!$cell) {
                    $cell = new Cell($this, $row, $col);
                    $row->addCell($cell);
                } else {
                    $untouchedCells->detach($cell);
                }
            }

            foreach ($untouchedCells as $untouchedCell) {
                $row->removeCell($untouchedCell);
            }
        }
    }

    public function getAdditionalData(): string
    {
        return $this->additionalData;
    }

    public function setAdditionalData(string $additionalData): void
    {
        $this->additionalData = $additionalData;
    }
}