<?php

namespace App\Entity\FormPrototype;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Row
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Tables", inversedBy="rows")
     * @ORM\JoinColumn(nullable=false)
     */
    private Tables $table;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $rowName = '';

    /**
     * @var Collection|Cell[]
     * @ORM\OneToMany(targetEntity="App\Entity\FormPrototype\Cell", mappedBy="row", orphanRemoval=true)
     */
    private Collection $cells;

    public function __construct(Tables $table)
    {
        $this->table = $table;
        $this->cells = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTable(): Tables
    {
        return $this->table;
    }

    public function getCells(): Collection
    {
        return $this->cells;
    }

    public function addCell(Cell $cell): void
    {
        $this->cells->add($cell);
    }

    public function removeCell(Cell $cell): void
    {
        $this->cells->removeElement($cell);
    }

    public function getCellForCol(Col $col): ?Cell
    {
        foreach ($this->cells as $cell) {
            if ($cell->getColumn()->equals($col)) {
                return $cell;
            }
        }
        return null;
    }

    public function getRowName(): string
    {
        return $this->rowName;
    }

    public function setRowName(string $rowName): void
    {
        $this->rowName = $rowName;
    }
}