<?php

namespace App\Entity\FormPrototype;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Col
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Tables", inversedBy="cols")
     * @ORM\JoinColumn(nullable=false)
     */
    private Tables $table;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $colName = '';

    public function __construct(Tables $table)
    {
        $this->table = $table;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTable(): Tables
    {
        return $this->table;
    }

    public function getColName(): string
    {
        return $this->colName;
    }

    public function setColName(string $colName): void
    {
        $this->colName = $colName;
    }

    public function equals(Col $col): bool
    {
        return $this === $col || $this->getId() === $col->getId();
    }
}