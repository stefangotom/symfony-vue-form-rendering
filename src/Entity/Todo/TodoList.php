<?php

namespace App\Entity\Todo;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"todo_list:read"}},
 *     denormalizationContext={"groups"={"todo_list:write"}},
 * )
 */
class TodoList
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"todo_list:read"})
     */
    private ?int $id;

    /**
     * @var Collection|TodoItem[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Todo\TodoItem", mappedBy="list", cascade={"all"}, orphanRemoval=true)
     * @ORM\JoinColumn()
     * @ApiSubresource()
     * @Groups({"todo_list:read"})
     */
    private Collection $items;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"todo_list:read", "todo_list:write"})
     */
    private string $name = '';

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function addItem(TodoItem $item): void
    {
        $this->items->add($item);
    }

    public function removeItem(TodoItem $item): void
    {
        $this->items->removeElement($item);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function __sleep()
    {
        // prevents the following error
        // Typed property Proxies\__CG__\App\Entity\Todo\TodoList::$ must not be accessed before initialization (in __sleep)
        return [];
    }
}