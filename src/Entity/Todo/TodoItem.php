<?php

namespace App\Entity\Todo;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 *
 * @ApiResource(
 *     normalizationContext={"groups": {"todo_item:read"}},
 *     denormalizationContext={"groups": {"todo_item:write"}},
 * )
 */
class TodoItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"todo_list:read", "todo_item:read"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Todo\TodoList", inversedBy="items")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Groups({"todo_item:read", "todo_item:write"})
     */
    private TodoList $list;

    /**
     * @ORM\Column(type="string")
     * @Groups({"todo_list:read", "todo_item:read", "todo_item:write"})
     */
    private string $title = '';

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"todo_list:read", "todo_item:read", "todo_item:write"})
     */
    private bool $done = false;

    public function __construct(TodoList $list)
    {
        $this->list = $list;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getList(): TodoList
    {
        return $this->list;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function isDone(): bool
    {
        return $this->done;
    }

    public function setDone(bool $done): void
    {
        $this->done = $done;
    }
}