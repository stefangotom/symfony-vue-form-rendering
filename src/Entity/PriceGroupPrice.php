<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class PriceGroupPrice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PriceGroup", inversedBy="prices")
     * @ORM\JoinColumn(nullable=false)
     */
    private PriceGroup $priceGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RateOption")
     * @ORM\JoinColumn(nullable=false)
     */
    private RateOption $rateOption;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExtraPriceLevel")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?ExtraPriceLevel $priceLevel;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private ?float $price = null;

    public function __construct(PriceGroup $priceGroup, RateOption $rateOption, ?ExtraPriceLevel $priceLevel)
    {
        $this->priceGroup = $priceGroup;
        $this->rateOption = $rateOption;
        $this->priceLevel = $priceLevel;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriceKey(): string
    {
        $id = $this->getId();
        if ($id !== null) {
            return (string)$id;
        }

        return sprintf('%s_%s', $this->priceLevel ? $this->priceLevel->getId() : '', $this->rateOption->getId());
    }

    public function getPriceGroup(): PriceGroup
    {
        return $this->priceGroup;
    }

    public function getRateOption(): RateOption
    {
        return $this->rateOption;
    }

    public function getPriceLevel(): ?ExtraPriceLevel
    {
        return $this->priceLevel;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    public function isEmpty(): bool
    {
        return $this->price === null;
    }
}