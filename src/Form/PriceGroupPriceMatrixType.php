<?php

namespace App\Form;

use App\Entity\ExtraPriceLevel;
use App\Entity\PriceGroup;
use App\Entity\PriceGroupPrice;
use App\Entity\RateOption;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceGroupPriceMatrixType extends AbstractType
{
    /**
     * @param iterable $data
     * @param PriceGroup $priceGroup
     * @param iterable|RateOption[] $rateOptions
     * @param iterable|ExtraPriceLevel[] $priceLevels
     * @return iterable
     */
    private static function buildCompleteMatrix(
        iterable $data,
        PriceGroup $priceGroup,
        iterable $rateOptions,
        iterable $priceLevels
    ): iterable
    {
        $resultMatrix = [];

        /** @var PriceGroupPrice $price */
        foreach ($data as $price) {
            [$rowId, $columnId] = self::getMatrixIds($price->getPriceLevel(), $price->getRateOption());
            $resultMatrix[$rowId][$columnId] = $price;
            $priceGroup = $price->getPriceGroup();
        }

        foreach ($priceLevels as $priceLevel) {
            foreach ($rateOptions as $rateOption) {
                [$rowId, $columnId] = self::getMatrixIds($priceLevel, $rateOption);

                if (isset($resultMatrix[$rowId][$columnId])) {
                    continue;
                }

                $resultMatrix[$rowId][$columnId] = new PriceGroupPrice($priceGroup, $rateOption, $priceLevel);
            }
        }

        $result = [];

        foreach ($resultMatrix as $matrixRow) {
            foreach ($matrixRow as $price) {
                $id = $price->getId();
                if ($id === null) {
                    $id = implode('_', self::getMatrixIds($price->getPriceLevel(), $price->getRateOption()));
                }

                $result[$id] = $price;
            }
        }

        return $result;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function (FormEvent $event) use ($options): void {
            $data = $event->getData();

            if (null === $data) {
                $data = [];
            }

            $completeMatrix = self::buildCompleteMatrix(
                $data,
                $options['matrix_price_group'],
                $options['matrix_rate_options'],
                $options['matrix_extra_price_levels']
            );

            $event->setData($completeMatrix);
        }, 1);
    }

    /**
     * @param ExtraPriceLevel|null $extraPriceLevel
     * @param RateOption $rateOption
     * @return string[]
     */
    private static function getMatrixIds(?ExtraPriceLevel $extraPriceLevel, RateOption $rateOption): array
    {
        return [(string)($extraPriceLevel ? $extraPriceLevel->getId() : ''), (string)$rateOption->getId()];
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $matrix = [];
        foreach ($view->children as $child) {
            /** @var PriceGroupPrice $price */
            $price = $child->vars['data'];

            [$rowId, $columnId] = self::getMatrixIds($price->getPriceLevel(), $price->getRateOption());

            $matrix['columns'][$columnId] = $price->getRateOption();
            $matrix['rows'][$rowId]['rowObject'] = $price->getPriceLevel();
            $matrix['rows'][$rowId]['columns'][$columnId] = $child;
        }

        // sort
        uasort($matrix['columns'], static function(RateOption $a, RateOption $b): int {
            return strnatcasecmp($a->getName(), $b->getName());
        });

        $view->vars['matrix'] = $matrix;
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('matrix_rate_options');
        $resolver->setAllowedTypes('matrix_rate_options', \iterable::class);
        $resolver->setRequired('matrix_extra_price_levels');
        $resolver->setAllowedTypes('matrix_extra_price_levels', \iterable::class);

        $resolver->setRequired('matrix_price_group');
        $resolver->setAllowedTypes('matrix_price_group', PriceGroup::class);

        $resolver->setDefaults([
            'entry_type' => PriceGroupPriceType::class,
            'entry_options' => [
                'label' => false,
            ],
            'allow_add' => true,
            'allow_remove' => true,
            'delete_empty' => static function(PriceGroupPrice $price): bool  {
                return $price->isEmpty();
            }
        ]);
    }

    public function getParent(): string
    {
        return CollectionType::class;
    }
}