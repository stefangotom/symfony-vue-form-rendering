<?php

namespace App\Form;

use App\Entity\PriceGroup;
use App\Repository\ExtraPriceLevelRepository;
use App\Repository\RateOptionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceGroupType extends AbstractType
{
    private RateOptionRepository $rateOptionRepository;
    private ExtraPriceLevelRepository $extraPriceLevelRepository;

    public function __construct(
        RateOptionRepository $rateOptionRepository,
        ExtraPriceLevelRepository $extraPriceLevelRepository
    ) {
        $this->rateOptionRepository = $rateOptionRepository;
        $this->extraPriceLevelRepository = $extraPriceLevelRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'label' => 'Name'
        ]);

        $builder->add('prices', PriceGroupPriceMatrixType::class, [
            'matrix_price_group' => $options['price_group'],
            'matrix_rate_options' => $this->rateOptionRepository->findAll(),
            'matrix_extra_price_levels' => [null, ...$this->extraPriceLevelRepository->findAll()],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('price_group');
        $resolver->setAllowedTypes('price_group', PriceGroup::class);

        $resolver->setDefaults([
            'data_class' => PriceGroup::class
        ]);
    }
}