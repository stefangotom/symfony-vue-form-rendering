<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerWithAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('customer', EntityType::class, [
            'class' => Customer::class,
            'choice_label' => 'name',
        ]);

//        $builder->addEventListener(FormEvents::PRE_SUBMIT, static function(FormEvent $event) {
//
//            $event->getForm()->add('address', EntityType::class, [
//                'class' => CustomerAddress::class,
//                'choices' => [],
//            ]);
//        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}