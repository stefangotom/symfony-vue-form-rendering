<?php

namespace App\Form\FormTest;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubSubType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('glucks', NumberType::class, [

        ]);

        $builder->add('gacks', NumberType::class, [

        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}