<?php

namespace App\Form\FormTest;

use App\Entity\Customer;
use App\Entity\Topics;
use App\Form\AjaxChoice\CustomEntityAjaxChoiceType;
use App\Form\CustomerWithAddressType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('textField', TextType::class, [
            'label' => 'hallo welt symfony form',
            'required' => true,
            'constraints' => [
                new NotBlank(),
                new Length(['min' => 5]),
            ]
        ]);
        $builder->add('numberField', NumberType::class, []);
        $builder->add('singleChoiceField', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Blub-Label' => 'Blub-Value',
                'Bla-Label' => 'Bla-Value',
            ],
        ]);

        $builder->add('entityField', EntityType::class, [
            'class' => Topics::class,
            'required' => true,
            'choice_label' => 'name',
            'constraints' => [
                new NotNull(),
            ],
        ]);

        $builder->add('gopfField', CustomerWithAddressType::class, [

        ]);

        $builder->add('optionalChoiceField', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'Blub-Label' => 'Blub-Value',
                'Bla-Label' => 'Bla-Value',
            ],
        ]);

        $builder->add('ajaxChoiceField', CustomEntityAjaxChoiceType::class, [
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => 'name',
        ]);

        $builder->add('ajaxChoiceFieldMulti', CustomEntityAjaxChoiceType::class, [
            'multiple' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => 'name',
        ]);

        $builder->add('ajaxChoiceFieldWithPreselection', CustomEntityAjaxChoiceType::class, [
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => 'name',
        ]);

        $builder->add('ajaxChoiceFieldMultiWithPreselection', CustomEntityAjaxChoiceType::class, [
            'multiple' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => 'name',
        ]);

        $builder->add($builder->create('subform', FormType::class)
            ->add('name', TextType::class)
            ->add('email', EmailType::class));

        $builder->add('coll', CollectionType::class, [
            'entry_type' => SubFormType::class,
            'entry_options' => [],
            'allow_add' => true,
            'allow_delete' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}