<?php

namespace App\Form\Table;

use App\Entity\FormPrototype\Row;
use App\Entity\FormPrototype\Tables;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, static function(FormEvent $event) {
            $rowsChildren = $event->getForm()->get('rows');
            $rows = $rowsChildren->getData();
            $toRemove = [];
            foreach ($rowsChildren as $name => $rowsChild) {
                if (!isset($rows[$name])) {
                    $toRemove[] = $name;
                }
            }

            foreach ($toRemove as $name) {
                $rowsChildren->remove($name);
            }
        });
        $builder->add('rows', CollectionType::class, [
            'entry_type' => TableRowType::class,
            'entry_options' => [
                'translation_domain' => false,
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tables::class
        ]);
    }
}