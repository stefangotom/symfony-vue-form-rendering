<?php

namespace App\Form\Table;

use App\Entity\FormPrototype\Col;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableDimensionEditColType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('colName', TextType::class, [

        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Col::class,
            'empty_data' => static function(FormInterface $form) {
                return new Col($form->getRoot()->getData());
            },
        ]);
    }
}