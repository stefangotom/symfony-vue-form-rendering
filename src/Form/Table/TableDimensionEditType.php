<?php

namespace App\Form\Table;

use App\Entity\FormPrototype\Col;
use App\Entity\FormPrototype\Row;
use App\Entity\FormPrototype\Tables;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableDimensionEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('rows', CollectionType::class, [
            'entry_type' => TableDimensionEditRowType::class,
            'allow_add' => true,
            'allow_delete' => true,
        ]);

        $builder->add('cols', CollectionType::class, [
            'entry_type' => TableDimensionEditColType::class,
            'allow_add' => true,
            'allow_delete' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tables::class,
        ]);
    }
}