<?php

namespace App\Form\Table;

use App\Entity\FormPrototype\Tables;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FullTableEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dimensionEdit', TableDimensionEditType::class, [
            'inherit_data' => true,
        ]);

        $builder->add('dataEdit', TableType::class, [
            'inherit_data' => true,
        ]);

        $builder->add('additionalData', TextType::class, []);
        $builder->add('testValue', TextType::class, ['mapped' => false, 'data' => 'stefan']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tables::class,
        ]);
    }
}