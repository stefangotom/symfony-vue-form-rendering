<?php

namespace App\Form\AjaxChoice;

use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use FOS\ElasticaBundle\Repository;
use Symfony\Bridge\Doctrine\Form\ChoiceList\IdReader;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Bridge\Doctrine\Form\EventListener\MergeDoctrineCollectionListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\ChoiceList\View\ChoiceListView;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AjaxChoiceType extends AbstractType
{
    protected ManagerRegistry $registry;
    protected UrlGeneratorInterface $urlGenerator;
    protected ChoiceListFactoryInterface $choiceListFactory;
    private RepositoryManagerInterface $repositoryManager;

    public function __construct(
        ManagerRegistry $registry,
        UrlGeneratorInterface $urlGenerator,
        ChoiceListFactoryInterface $choiceListFactory,
        RepositoryManagerInterface $repositoryManager
    ) {
        $this->registry = $registry;
        $this->urlGenerator = $urlGenerator;
        $this->choiceListFactory = $choiceListFactory;
        $this->repositoryManager = $repositoryManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['multiple']) {
            $builder
                ->addEventSubscriber(new MergeDoctrineCollectionListener())
                ->addViewTransformer(new CollectionToArrayTransformer(), true);
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $formType = get_class($form->getRoot()->getConfig()->getType()->getInnerType());
        $propertyPath = $form->getPropertyPath()->getElements();

        $ajaxEndpoint = $this->urlGenerator->generate('app_ajaxsearch_getoptions', ['formType' => $formType, 'propertyPath' => $propertyPath]);
        $view->vars['ajax_endpoint'] = $ajaxEndpoint;

        $choices = $form->getData() ?? [];
        if ($choices instanceof Collection) {
            $choices = $choices->toArray();
        } elseif (is_object($choices)) {
            $choices = [$choices];
        }
        $choicesList = $this->choiceListFactory->createListFromChoices($choices, $options['choice_value']);
        $choicesListView = $this->createChoiceListView($choicesList, $options);

        $view->vars = array_replace($view->vars, [
            'ajax_endpoint' => $ajaxEndpoint,
            'choices' => $choicesListView->choices,
        ]);

        $view->vars['attr']['data-ajax-endpoint'] = $ajaxEndpoint;
    }

    protected function createChoiceLoader(Options $options): AjaxChoiceLoaderInterface
    {
        if ($options['elastica']) {
            return new ElasticaChoiceLoader($this->choiceListFactory, $options['repository'], $options['id_reader'], $options['choice_label']);
        }

        $queryBuilder = $options['query_builder'];

        if ($queryBuilder === null) {
            $queryBuilder = $options['em']->getRepository($options['class'])->createQueryBuilder('e');
        }

        return new EntityChoiceLoader($this->choiceListFactory, $queryBuilder, $options['id_reader'], $options['choice_label'], $options['query_builder_search']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $choiceLoader = function (Options $options) {
            if ($options['choices'] !== null) {
                return null;
            }

            return $this->createChoiceLoader($options);
        };

        $resolver->setDefaults([
            'choices' => null,
            'choice_loader' => $choiceLoader,
            'elastica' => false,
        ]);

        $this->configureOptionsEntity($resolver);
        $this->configureOptionsElastica($resolver);
    }

    private function configureOptionsEntity(OptionsResolver $resolver): void
    {
        // The choices are always indexed by ID (see "choices" normalizer
        // and DoctrineChoiceLoader), unless the ID is composite. Then they
        // are indexed by an incrementing integer.
        // Use the ID/incrementing integer as choice value.
        $choiceValue = static function (Options $options) {
            // If the entity has a single-column ID, use that ID as value
            if ($options['id_reader'] instanceof IdReader && $options['id_reader']->isSingleId()) {
                return [$options['id_reader'], 'getIdValue'];
            }

            // Otherwise, an incrementing integer is used as value automatically
            return null;
        };

        $emNormalizer = function (Options $options, $em) {
            if (null !== $em) {
                if ($em instanceof ObjectManager) {
                    return $em;
                }

                return $this->registry->getManager($em);
            }

            $em = $this->registry->getManagerForClass($options['class']);

            if (null === $em) {
                throw new RuntimeException(sprintf('Class "%s" seems not to be a managed Doctrine entity. Did you forget to map it?', $options['class']));
            }

            return $em;
        };

        // Invoke the query builder closure so that we can cache choice lists
        // for equal query builders
        $queryBuilderNormalizer = static function (Options $options, $queryBuilder) {
            if (\is_callable($queryBuilder)) {
                $queryBuilder = $queryBuilder($options['em']->getRepository($options['class']));
            }

            return $queryBuilder;
        };

        $idReaderNormalizer = static function (Options $options) {
            $classMetadata = $options['em']->getClassMetadata($options['class']);
            $idReader = new IdReader($options['em'], $classMetadata);

            if ($idReader->isSingleId()) {
                return $idReader;
            }

            return null;
        };

        $resolver->setDefaults([
            'em' => null,
            'query_builder' => null,
            'id_reader' => null,
            'choice_value' => $choiceValue,
            'query_builder_search' => 'name',
        ]);
        $resolver->setRequired(['class']);

        $resolver->setNormalizer('em', $emNormalizer);
        $resolver->setNormalizer('query_builder', $queryBuilderNormalizer);
        $resolver->setNormalizer('id_reader', $idReaderNormalizer);

        $resolver->setAllowedTypes('em', ['null', 'string', ObjectManager::class]);
        $resolver->setAllowedTypes('query_builder_search', ['callable', 'string']);
    }

    private function configureOptionsElastica(OptionsResolver $resolver): void
    {
        // The choices are always indexed by ID (see "choices" normalizer
        // and DoctrineChoiceLoader), unless the ID is composite. Then they
        // are indexed by an incrementing integer.
        // Use the ID/incrementing integer as choice value.
        $choiceValue = static function (Options $options) {
            // If the entity has a single-column ID, use that ID as value
            if ($options['id_reader'] instanceof IdReader && $options['id_reader']->isSingleId()) {
                return [$options['id_reader'], 'getIdValue'];
            }

            // Otherwise, an incrementing integer is used as value automatically
            return null;
        };

        $repositoryNormalizer = function (Options $options, $finder) {
            if (null !== $finder) {
                if ($finder instanceof PaginatedFinderInterface) {
                    $finder = new Repository($finder);
                }
                return $finder;
            }

            return $this->repositoryManager->getRepository($options['class']);
        };

        $idReaderNormalizer = static function (Options $options) {
            $classMetadata = $options['em']->getClassMetadata($options['class']);
            $idReader = new IdReader($options['em'], $classMetadata);

            if ($idReader->isSingleId()) {
                return $idReader;
            }

            return null;
        };

        $resolver->setDefaults([
            'repository' => null,
            'id_reader' => null,
            'choice_value' => $choiceValue,
        ]);
        $resolver->setRequired(['class']);

        $resolver->setNormalizer('repository', $repositoryNormalizer);
        $resolver->setNormalizer('id_reader', $idReaderNormalizer);

        $resolver->setAllowedTypes('repository', ['null', Repository::class, 'string', PaginatedFinderInterface::class]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }

    private function createChoiceListView(ChoiceListInterface $choiceList, array $options): ChoiceListView
    {
        return $this->choiceListFactory->createView(
            $choiceList,
            $options['preferred_choices'],
            $options['choice_label'],
            $options['choice_name'],
            $options['group_by'],
            $options['choice_attr']
        );
    }
}