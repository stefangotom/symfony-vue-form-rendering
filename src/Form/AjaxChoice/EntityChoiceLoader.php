<?php

namespace App\Form\AjaxChoice;

use Doctrine\ORM\QueryBuilder;
use Elastica\Query;
use Symfony\Bridge\Doctrine\Form\ChoiceList\IdReader;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;

class EntityChoiceLoader implements AjaxChoiceLoaderInterface
{
    private ChoiceListFactoryInterface $choiceListFactory;
    private QueryBuilder $queryBuilder;
    private $label;
    private IdReader $idReader;
    /** @var callable */
    private $searchBuilder;

    public function __construct(
        ChoiceListFactoryInterface $choiceListFactory,
        QueryBuilder $queryBuilder,
        IdReader $idReader,
        $label,
        $searchBuilder
    ) {
        $this->queryBuilder = $queryBuilder;
        $this->choiceListFactory = $choiceListFactory;
        $this->label = $label;
        $this->idReader = $idReader;
        $this->searchBuilder = is_string($searchBuilder) ? self::createSearchBuilderFunction($searchBuilder) : $searchBuilder;
    }

    private const SEARCH_STRING_PARAMETER_NAME = '_EntityChoiceLoaderSearchString';

    private static function createSearchBuilderFunction(string $fieldName): callable
    {
        return static function (string $searchString, QueryBuilder $qb) use ($fieldName) {
            $qb->andWhere(
                $qb->expr()->like(
                    self::getAliasedFieldname($qb, $fieldName),
                    ':' . self::SEARCH_STRING_PARAMETER_NAME
                )
            );
            $qb->setParameter(self::SEARCH_STRING_PARAMETER_NAME, '%' . $searchString . '%');
        };
    }

    public function findChoices(string $searchString, int $offset, int $limit, callable $value = null): array
    {
        $qb = clone $this->queryBuilder;

        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset * $limit);

        if ($searchString) {
            $searchFn = $this->searchBuilder;
            $result = $searchFn($searchString, $qb);
            if ($result instanceof QueryBuilder) {
                $qb = $result;
            }
        }

        $resultSet = $qb->getQuery()->getResult();

        if ($value === null) {
            $i = 0;
            $value = static function () use (&$i) { return $i++; };
        }

        $choiceList = $this->choiceListFactory->createListFromChoices($resultSet, $value);
        $choices = $this->choiceListFactory->createView($choiceList, null, $this->label);

        return $choices->choices;
    }

    public function loadChoiceList(callable $value = null): ChoiceListInterface
    {
        return new ArrayChoiceList([], $value);
    }

    public function loadChoicesForValues(array $values, callable $value = null): array
    {
        $qb = clone $this->queryBuilder;

        $qb->andWhere($qb->expr()->in(
            self::getAliasedFieldname($qb, $this->idReader->getIdField()),
            ':entityIds'
        ));

        $values = array_filter($values);
        $qb->setParameter('entityIds', $values);

        $entities = $qb->getQuery()->getResult();

        $result = [];

        foreach ($entities as $entity) {
            $entityValue = $value($entity);

            $result[$entityValue] = $entity;
        }

        return $result;
    }

    public function loadValuesForChoices(array $choices, callable $value = null): array
    {
        $result = [];

        foreach ($choices as $index => $choice) {
            if (!$choice) {
                continue;
            }
            $result[$index] = (string)$this->idReader->getIdValue($choice);
        }

        return $result;
    }

    public static function getAliasedFieldname(QueryBuilder $qb, string $name): string
    {
        $alias = current($qb->getRootAliases());

        return sprintf('%s.%s', $alias, $name);
    }
}