<?php

namespace App\Form\AjaxChoice;

use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;

interface AjaxChoiceLoaderInterface extends ChoiceLoaderInterface
{
    public function findChoices(string $searchString, int $offset, int $limit, callable $value = null): array;
}