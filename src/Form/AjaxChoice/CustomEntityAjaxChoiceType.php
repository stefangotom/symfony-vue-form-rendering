<?php

namespace App\Form\AjaxChoice;

class CustomEntityAjaxChoiceType extends AbstractCustomEntityAjaxChoiceType
{
    public function getParent(): string
    {
        return AjaxChoiceType::class;
    }
}