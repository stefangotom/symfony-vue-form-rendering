<?php

namespace App\Form\AjaxChoice;

use App\Entity\Customer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsFalse;

class AjaxTestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $fancyLabel = static function (Customer $customer) {
            $result = $customer->getName();

            $addresses = $customer->getAddresses();
            if (count($addresses) > 0) {
                $address = $addresses[0];
                $houseNumber = $address->getHouseNumber();
                $result .= sprintf(
                    ' [%s]: %s, %s, %s',
                    $address->getDesignation(),
                    $address->getStreet() . ($houseNumber ? ' ' . $houseNumber : '') ,
                    $address->getZip(),
                    $address->getCity()
                );
            }

            return $result;
        };

        $builder->add('customer', EntityAjaxChoiceType::class, [
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('customerList', EntityAjaxChoiceType::class, [
            'multiple' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('customerElastica', AjaxChoiceType::class, [
            'elastica' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('customerElasticaMulti', AjaxChoiceType::class, [
            'multiple' => true,
            'elastica' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('extendedCustomer', CustomEntityAjaxChoiceType::class, [
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('extendedCustomerList', CustomEntityAjaxChoiceType::class, [
            'multiple' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('extendedCustomerElastica', CustomEntityAjaxChoiceType::class, [
            'elastica' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('extendedCustomerElasticaMulti', CustomEntityAjaxChoiceType::class, [
            'multiple' => true,
            'elastica' => true,
            'class' => Customer::class,
            'choice_value' => 'id',
            'choice_name' => 'id',
            'choice_label' => $fancyLabel,
        ]);

        $builder->add('customer1', EntityType::class, [
            'class' => Customer::class,
            'choice_label' => 'name',
        ]);

        $builder->add('customer2', ChoiceType::class, [
            'choices' => $this->getChoices(),
            'choice_label' => 'name',
        ]);

        $builder->add('failTest', CheckboxType::class, [
            'required' => false,
            'constraints' => [new IsFalse()],
        ]);
    }

    private function getChoices(): array
    {
        $customer1 = new Customer();
        $customer1->setName('Fritz');

        $customer2 = new Customer();
        $customer2->setName('Hans');

        $customer3 = new Customer();
        $customer3->setName('Heiri');

        return [$customer1, $customer2, $customer3];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}