<?php

namespace App\Form\AjaxChoice;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class AbstractCustomEntityAjaxChoiceType extends AbstractType
{
    /**
     * @param mixed $data
     * @param ChoiceView[] $choices
     * @param bool $multiple
     * @return null|ChoiceView|ChoiceView[]
     */
    private static function getDataAsChoice($data, iterable $choices, bool $multiple)
    {
        if ($multiple) {
            if ($data === null) {
                return [];
            }

            $result = [];
            foreach ($data as $dataItem) {
                $dataView = self::getDataAsChoice($dataItem, $choices, false);
                if ($dataView) {
                    $result[] = $dataView;
                }
            }

            return $result;
        }

        if ($data === null) {
            return null;
        }

        foreach ($choices as $choice) {
            if ($choice->data === $data) {
                return $choice;
            }
        }

        return null;
    }

    public function getBlockPrefix(): string
    {
        return 'ajax_choice_type';
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['choices_array'] = array_values($view->vars['choices']);
        $view->vars['data_as_choices'] = self::getDataAsChoice($form->getData(), $view->vars['choices'], $view->vars['multiple']);
    }
}