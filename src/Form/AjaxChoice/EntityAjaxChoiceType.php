<?php

namespace App\Form\AjaxChoice;

use Symfony\Component\OptionsResolver\Options;

class EntityAjaxChoiceType extends AjaxChoiceType
{
    protected function createChoiceLoader(Options $options): AjaxChoiceLoaderInterface
    {
        $queryBuilder = $options['query_builder'];

        if ($queryBuilder === null) {
            $queryBuilder = $options['em']->getRepository($options['class'])->createQueryBuilder('e');
        }

        return new EntityChoiceLoader($this->choiceListFactory, $queryBuilder, $options['id_reader'], $options['choice_label'], $options['query_builder_search']);
    }
}