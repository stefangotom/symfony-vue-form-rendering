<?php

namespace App\Form\AjaxChoice;

use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchAll;
use FOS\ElasticaBundle\Repository;
use Symfony\Bridge\Doctrine\Form\ChoiceList\IdReader;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;

class ElasticaChoiceLoader implements AjaxChoiceLoaderInterface
{
    private ChoiceListFactoryInterface $choiceListFactory;
    private Repository $repository;
    private $label;
    private IdReader $idReader;

    public function __construct(
        ChoiceListFactoryInterface $choiceListFactory,
        Repository $repository,
        IdReader $idReader,
        $label
    ) {
        $this->choiceListFactory = $choiceListFactory;
        $this->label = $label;
        $this->idReader = $idReader;
        $this->repository = $repository;
    }

    public function findChoices(string $searchString, int $offset, int $limit, callable $value = null): array
    {
        $query = new Query\QueryString('*' . $searchString . '*');

        $resulsts = $this->repository->find($query, $limit, ['from' => $offset]);

        if ($value === null) {
            $i = 0;
            $value = static function () use (&$i) { return $i++; };
        }

        $choiceList = $this->choiceListFactory->createListFromChoices($resulsts, $value);
        $choices = $this->choiceListFactory->createView($choiceList, null, $this->label);

        return $choices->choices;
    }

    public function loadChoiceList(callable $value = null): ChoiceListInterface
    {
        return new ArrayChoiceList([], $value);
    }

    public function loadChoicesForValues(array $values, callable $value = null): array
    {
        $boolQuery = new BoolQuery();

        $boolQuery->addShould(new MatchAll());

        $values = array_filter($values);

        $idQuery = new \Elastica\Query\Terms();
        $idQuery->setTerms('_id', $values);
        $boolQuery->addMust($idQuery);

        $entities = $this->repository->find($boolQuery);

        $result = [];

        foreach ($entities as $entity) {
            $entityValue = $value($entity);

            $result[$entityValue] = $entity;
        }

        return $result;
    }

    public function loadValuesForChoices(array $choices, callable $value = null): array
    {
        $result = [];

        foreach ($choices as $index => $choice) {
            if (!$choice) {
                continue;
            }
            $result[$index] = (string)$this->idReader->getIdValue($choice);
        }

        return $result;
    }

}