<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Comment;
use App\Entity\Conference;
use App\Entity\Customer;
use App\Entity\CustomerAddress;
use App\Entity\Todo\TodoItem;
use App\Entity\Todo\TodoList;
use App\Entity\Topics;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class AppFixtures extends Fixture
{
    private EncoderFactoryInterface $encoderFactory;
    private Generator $faker;

    public function __construct(EncoderFactoryInterface $encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;
        $this->faker = Factory::create('de_CH');
    }

    public function load(ObjectManager $manager)
    {
        $amsterdam = new Conference();
        $amsterdam->setCity('Amsterdam');
        $amsterdam->setYear('2019');
        $amsterdam->setIsInternational(true);
        $manager->persist($amsterdam);

        $paris = new Conference();
        $paris->setCity('Paris');
        $paris->setYear('2020');
        $paris->setIsInternational(false);
        $manager->persist($paris);

        $comment1 = new Comment();
        $comment1->setState('published');
        $comment1->setConference($amsterdam);
        $comment1->setAuthor('Fabien');
        $comment1->setEmail('fabien@example.com');
        $comment1->setText('This was a great conference.');
        $manager->persist($comment1);

        $comment2 = new Comment();
        $comment2->setConference($amsterdam);
        $comment2->setAuthor('Lucas');
        $comment2->setEmail('lucas@example.com');
        $comment2->setText('I think this oen is going to be moderated.');
        $manager->persist($comment2);


        $admin = new Admin();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setUsername('admin');
        $admin->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('admin', null));
        $manager->persist($admin);

        $this->loadTodos($manager);
        $this->loadTopics($manager);
        $this->loadCustomers($manager);

        $manager->flush();
    }

    public function loadTodos(ObjectManager $manager): void
    {
        $todoList = new TodoList();
        $todoList->setName('Test');


        $todoItem = new TodoItem($todoList);
        $todoItem->setTitle('Gopf kaufen');

        $todoItem1 = new TodoItem($todoList);
        $todoItem1->setTitle('Schwurbel kaufen');

        $manager->persist($todoList);
        $manager->persist($todoItem);
        $manager->persist($todoItem1);
    }

    public function loadTopics(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 20; ++$i) {
            $topic = new Topics('Test Topic ' . $i);
            $manager->persist($topic);
        }
    }

    private function createRandomAddress(Customer $customer, ?string $designation = null): CustomerAddress
    {
        $address = new CustomerAddress($customer);

        if (!$designation) {
            $designation = $this->faker->word;
        }

        $address->setDesignation($designation);
        $address->setCity($this->faker->city);
        $address->setZip($this->faker->postcode);
        $address->setStreet($this->faker->streetName);
        $address->setHouseNumber($this->faker->streetSuffix);
        return $address;
    }

    public function loadCustomers(ObjectManager $manager): void
    {
        for ($customerIndex = 0; $customerIndex < 5000; ++$customerIndex) {
            $customer = new Customer();
            $customer->setName($this->faker->company);

            $addressDesignations = ['Hauptadresse', 'Rechnungsadresse', ''];
            $numberOfAddresses = $this->faker->numberBetween(1, 3);
            for ($addressIndex = 0; $addressIndex < $numberOfAddresses; ++$addressIndex) {
                $address = $this->createRandomAddress($customer, $addressDesignations[$addressIndex] ?? null);
                $manager->persist($address);
            }
            $manager->persist($customer);
        }

        $fixed = ['Stefan AG', 'Stefan GmbH', 'Gopf GmbH', 'Zapf AG'];

        foreach ($fixed as $name) {
            $customer = new Customer();
            $customer->setName($name);
            $address = $this->createRandomAddress($customer, 'Hauptadresse');
            $manager->persist($customer);
            $manager->persist($address);
        }

    }
}
