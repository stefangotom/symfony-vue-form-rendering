<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\PriceGroup;
use App\Form\PriceGroupType;
use App\Message\CommentMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Registry;
use Twig\Environment;

class AdminController extends AbstractController
{
    private Environment $twig;
    private EntityManagerInterface $entityManager;
    private MessageBusInterface $bus;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager, MessageBusInterface $bus)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->bus = $bus;
    }

    /**
     * @Route("/admin/comment/review/{id}", name="review_comment")
     *
     * @param Request $request
     * @param Comment $comment
     * @param Registry $registry
     * @return Response
     */
    public function reviewComment(Request $request, Comment $comment, Registry $registry): Response
    {
        $accepted = !$request->query->get('reject');
        $machine = $registry->get($comment);

        if ($machine->can($comment, 'publish')) {
            $transition = $accepted ? 'publish' : 'reject';
        } elseif ($machine->can($comment, 'publish_ham')) {
            $transition = $accepted ? 'publish_ham' : 'reject_ham';
        } else {
            return new Response('Comment already reviewed or not in the right state.');
        }

        $machine->apply($comment, $transition);
        $this->entityManager->flush();
        if ($accepted) {
            $this->bus->dispatch(new CommentMessage($comment->getId()));
        }

        return $this->render('admin/review.html.twig', [
            'transition' => $transition,
            'comment' => $comment,
        ]);
    }


    /**
     * @Route("/admin/price-group/{id}", name="edit_price_group")
     *
     * @param Request $request
     * @return Response
     */
    public function editPriceGroup(Request $request, PriceGroup $priceGroup): Response
    {
        $form = $this->createForm(PriceGroupType::class, $priceGroup, [
            'price_group' => $priceGroup
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($priceGroup->getPrices() as $price) {
                if (!$price->isEmpty()) {
                    $this->entityManager->persist($price);
                } else {
                    $priceGroup->getPrices()->removeElement($price);
                    $this->entityManager->remove($price);
                }
            }

            $this->entityManager->flush();

            return $this->redirectToRoute('edit_price_group', [
                'id' => $priceGroup->getId(),
            ]);
        }

        return $this->render('admin/edit-pricegroup.html.twig', [
            'priceGroup' => $priceGroup,
            'priceGroupForm' => $form->createView(),
        ]);
    }
}