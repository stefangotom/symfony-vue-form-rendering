<?php

namespace App\Controller;

use App\Form\AjaxChoice\AjaxChoiceLoaderInterface;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax-search-controller")
 * Class AjaxSearchController
 * @package App\Controller
 */
class AjaxSearchController
{
    private FormFactoryInterface $formFactory;
    private ChoiceListFactoryInterface $choiceListFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/{formType}", methods={"GET"})
     * @param Request $request
     * @param string $formType
     * @return Response
     */
    public function getOptions(Request $request, string $formType): Response
    {
        $form = $this->formFactory->create($formType);

        $propertyPath = (array)$request->query->get('propertyPath');

        $subForm = $form;
        foreach ($propertyPath as $pathElem) {
            $subForm = $subForm->get($pathElem);
        }

        $loader = $subForm->getConfig()->getOption('choice_loader');

        if (!$loader instanceof AjaxChoiceLoaderInterface) {
            throw new BadRequestHttpException('Oh no');
        }

        $value = $subForm->getConfig()->getOption('choice_value');
        if (is_string($value)) {
            $value = new PropertyPath($value);
        }

        if ($value instanceof PropertyPath) {
            $accessor = new PropertyAccessor();
            $value = function ($choice) use ($accessor, $value) {
                // The callable may be invoked with a non-object/array value
                // when such values are passed to
                // ChoiceListInterface::getValuesForChoices(). Handle this case
                // so that the call to getValue() doesn't break.
                return \is_object($choice) || \is_array($choice) ? $accessor->getValue($choice, $value) : null;
            };
        }

        $choices = $loader->findChoices(
            $request->query->get('q', ''),
            $request->query->getInt('page', 1) - 1,
            max(min($request->query->getInt('limit', 100), 1000), 0),
            $value
        );

        return JsonResponse::create($choices);
    }
}