<?php


namespace App\Controller;


use App\Entity\Customer;
use App\Entity\FormPrototype\Tables;
use App\Form\AjaxChoice\AjaxChoiceType;
use App\Form\AjaxChoice\AjaxTestType;
use App\Form\FormTest\TestType;
use App\Form\Table\FullTableEditType;
use App\Form\Table\TableCreateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;


/**
 * @Route("/form-test", name="form_test_")
 *
 * @param Request $request
 * @return Response
 * @throws \Twig\Error\LoaderError
 * @throws \Twig\Error\RuntimeError
 * @throws \Twig\Error\SyntaxError
 */
class FormTestController
{
    private Environment $twig;
    private FormFactoryInterface $formFactory;
    private EntityManagerInterface $entityManager;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        Environment $twig,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
    }

    private function isSerializable($key, $value): bool
    {
        if ($key === 'prototype' && $value instanceof FormView) {
            return true;
        }

        if ($value instanceof FormErrorIterator) {
            return true;
        }

        if (is_scalar($value) || is_array($value) || $value === null) {
            return true;
        }

        if ($value instanceof ChoiceView) {
            return true;
        }

        return false;
    }

    /**
     * @param string $key
     * @param $var
     * @return mixed
     */
    private function serializeVar(string $key, $var)
    {
        if ($key === 'prototype') {
            return $this->serializePrototype($var);
        }

        if ($var instanceof FormErrorIterator) {
            return $this->serializeErrorIterator($var);
        }

        if (is_array($var)) {
            return $this->serializeVars($var);
        }

        if ($var instanceof ChoiceView) {
            return $this->serializeChoiceView($var);
        }

        return $var;
    }

    private function serializeVars(array $vars): array
    {
        $result = [];

        foreach ($vars as $key => $var) {
            if (!$this->isSerializable($key, $var)) {
                continue;
            }

            $result[$key] = $this->serializeVar($key, $var);
        }

        return $result;
    }

    private function serializeForm(FormView $formView): array
    {
        $result = [];

        $result['vars'] = $this->serializeVars($formView->vars);

        foreach ($formView->children as $key => $child) {
            $result['children'][$key] = $this->serializeForm($child);
        }

        return $result;
    }

    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(Request $request): Response
    {
        $customerRepo = $this->entityManager->getRepository(Customer::class);

        $testData = [
            'coll' => [
                ['textField' => 'hallo'],
                ['textField' => 'welt'],
            ],
            'ajaxChoiceFieldWithPreselection' => $customerRepo->findOneBy([]),
            'ajaxChoiceFieldMultiWithPreselection' => $customerRepo->findBy([], null, 3),
        ];
        $form = $this->formFactory->create(TestType::class, $testData);
        $form->handleRequest($request);

        $formView = $form->createView();
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['form' => $this->serializeForm($formView)]);
        }

        return new Response($this->twig->render('form_test/index.html.twig', [
            'form' => $formView,
            'formSerialized' => $this->serializeForm($formView),
        ]));
    }

    private function serializeChoiceView(ChoiceView $var): array
    {
        return [
            'label' => $var->label,
            'value' => $var->value,
            'data' => $this->serializeVar('', $var->data),
            'attr' => $this->serializeVars($var->attr),
        ];
    }

    private function serializePrototype(FormView $form): array
    {
        return $this->serializeForm($form);
    }

    private function serializeErrorIterator(FormErrorIterator $errors): array
    {
        $result = [];

        foreach ($errors as $error) {

            $origin = $error->getOrigin();

            if ($origin) {
                $origin = $this->serializeFormInterface($origin);
            }

            $result[] = [
                'cause' => $error->getCause(),
                'message' => $error->getMessage(),
                'messageTemplate' => $error->getMessageTemplate(),
                'messageParameters' => $this->serializeVars($error->getMessageParameters()),
                'messagePluralization' => $error->getMessagePluralization(),
                'origin' => $origin,
            ];
        }

        return $result;
    }

    private function serializeFormInterface(FormInterface $origin): array
    {
        return [
            'name' => $origin->getName(),
            'propertyPath' => $origin->getPropertyPath(),
        ];
    }

    /**
     * @Route("/ajax-test")
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function ajaxTest(Request $request): Response
    {
        $data = [
            'customer' => $this->entityManager->find(Customer::class, 16045),
            'customer1' => $this->entityManager->find(Customer::class, 16040),
            'customerList' => $this->entityManager->getRepository(Customer::class)->findBy(['id' => [16045, 16026, 16051]]),
        ];

        $form = $this->formFactory->create(AjaxTestType::class, $data, [
        ]);

        if ($request->isMethod('post')) {
            $form->handleRequest($request);
        }

        $formView = $form->createView();

        return new Response($this->twig->render('form_test/ajax-test.html.twig', [
            'form' => $formView,
            'formData' => $form->getData(),
        ]));
    }

    /**
     * @Route("/tables", methods={"GET", "POST"})
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function tableList(Request $request): Response
    {
        $tables = $this->entityManager->getRepository(Tables::class)->findAll();

        $newTable = new Tables();
        $tableCreateForm = $this->formFactory->create(TableCreateType::class, $newTable);

        $tableCreateForm->handleRequest($request);

        if ($tableCreateForm->isSubmitted() && $tableCreateForm->isValid()) {
            $this->entityManager->persist($newTable);
            $this->entityManager->flush();
            return new RedirectResponse($this->urlGenerator->generate('form_test_app_formtest_tablelist'));
        }

        return new Response($this->twig->render('form_test/table-list.html.twig', [
            'tables' => $tables,
            'createForm' => $tableCreateForm->createView()
        ]));
    }

    /**
     * @Route("/tables/{id}", methods={"GET", "POST"})
     * @param Request $request
     * @param Tables $table
     * @return Response
     */
    public function tableEdit(Request $request, Tables $table): Response
    {
        $GLOBALS['table'] = $table;
        $table->setAdditionalData((new \DateTimeImmutable())->format('c'));
        $tableEditForm = $this->formFactory->create(FullTableEditType::class, $table, []);

        $tableEditForm->handleRequest($request);
        $table->updateCells();
        $table->setAdditionalData((new \DateTimeImmutable())->format('c'));

        if ($tableEditForm->isSubmitted() && $tableEditForm->isValid()) {
            foreach ($table->getCols() as $col) {
                $this->entityManager->persist($col);
            }

            foreach ($table->getRows() as $row) {
                $this->entityManager->persist($row);

                foreach ($row->getCells() as $cell) {
                    $this->entityManager->persist($cell);
                }
            }

            $this->entityManager->flush();
            $tableEditForm = $this->formFactory->create(FullTableEditType::class, $table, []);

            if (!$request->isXmlHttpRequest()) {
                return new RedirectResponse(
                    $this->urlGenerator->generate(
                        'form_test_app_formtest_tableedit',
                        ['id' => $table->getId()]
                    )
                );
            }
        }

        $formView = $tableEditForm->createView();
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['form' => $this->serializeForm($formView)]);
        }

        return new Response($this->twig->render('form_test/table-edit.html.twig', [
            'form' => $formView,
            'formSerialized' => $this->serializeForm($formView),
        ]));
    }

    /**
     * @Route("/tables/{id}/dump", methods={"GET"})
     * @param Tables $table
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function tableDump(Tables $table): Response
    {
        return new Response($this->twig->render('form_test/table-dump.html.twig', [
            'table' => $table,
        ]));
    }
}