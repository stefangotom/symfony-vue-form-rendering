<?php

use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

if (getenv('PHPUNIT_NO_BOOTSTRAP') === '1') {
    return;
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$application = new Application($kernel);
$application->setAutoExit(false);
$application->run(new \Symfony\Component\Console\Input\ArrayInput([
    'doctrine:database:create',
    '--if-not-exists' => true,
]));

$application->run(new \Symfony\Component\Console\Input\ArrayInput([
    'doctrine:migrations:migrate',
    '--no-interaction' => true,
]));

$application->run(new \Symfony\Component\Console\Input\ArrayInput([
    'doctrine:fixtures:load',
    '--no-interaction' => true,
]));
