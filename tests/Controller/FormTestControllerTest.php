<?php

namespace App\Tests\Controller;

use App\Entity\FormPrototype\Row;
use App\Entity\FormPrototype\Tables;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FormTestControllerTest extends WebTestCase
{
    public function testTableEdit()
    {
        $client = static::createClient();

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $table = new Tables();
        $em->persist($table);
        foreach (['Row 1', 'Row 2', 'Row 3'] as $rowName) {
            $row = new Row($table);
            $row->setRowName($rowName);
            $table->addRow($row);
            $em->persist($row);
        }

        $em->flush();

        $client->request('GET', '/form-test/tables/' . $table->getId());
        $buttonNode = $client->getCrawler()->selectButton('Hidden Submit');
        $form = $buttonNode->form(null, 'POST');
        $form->remove('full_table_edit[dimensionEdit][rows][2][rowName]');
        $client->submit($form);

        $reloaded = $em->find(Tables::class, $table->getId());
        static::assertCount(2, $reloaded->getRows());
    }
}
